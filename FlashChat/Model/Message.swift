//
//  Message.swift
//  FlashChat
//
//  Created by Gerson Costa on 26/10/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import Foundation

class Message {
    
    //TODO: Messages need a messageBody and a sender variable
    
    var sender: String = ""
    var messageBody: String = ""
    
}
